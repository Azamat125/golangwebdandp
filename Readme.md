<h1>Создание файла модуля Systemd</h1>

Необходимо создать файл модуля systemd, чтобы приложение работало в фоновом режиме, для устойчивости

<ol>
<li>sudo nano /lib/systemd/system/goweb.service 
   
</li>
<li>
[Unit]
<br>
Description=goweb
<br>
[Service]
<br>
Type=simple
<br>
Restart=always
<br>
RestartSec=5s
<br>
ExecStart=/home/user/go/go-web/main (путь к рабочему проекту)

[Install]
<br>
WantedBy=multi-user.target

</li>
<li>Сохранить и выйти</li>
<li>sudo service goweb start (Запустить веб-службу Go)
</li>
<li>sudo service goweb status  (Чтобы убедиться, что служба запущена)
</li>
</ol>
