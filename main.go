package main

import (
	"fmt"
	"html/template"
	"net/http"
	"serverMethods/methods"
)

func index(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("templates/index.html", "templates/header.html", "templates/footer.html")

	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	t.ExecuteTemplate(w, "index", nil)
}
func catalog(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("templates/catalog.html", "templates/header.html", "templates/footer.html","templates/modal.nord.html","templates/modal.alfa.html","templates/modal.zcool.html","templates/modal.nordBlue.html")

	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	t.ExecuteTemplate(w, "catalog", nil)
}
func about(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("templates/about.html", "templates/header.html", "templates/footer.html","templates/form.html")

	if err != nil {
		fmt.Fprintf(w, err.Error())
	}

	t.ExecuteTemplate(w, "about", nil)
}

func handlerFunc() {

	http.HandleFunc("/sendMail",methods.HandleMail)

	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))
	http.HandleFunc("/", index)
	http.HandleFunc("/catalog", catalog)
	http.HandleFunc("/about", about)
	http.ListenAndServe(":8080", nil)



}

func main() {

	handlerFunc()
}
